# Calibre Virtual Machine Setup With Ansible

## Introduction

A small Ansible playbook and script to create a Calibre server in a Ubuntu 18.04 VM (as this seems to be the easiest operating system to create one on). The idea of this is to run this on my server (running Fedora Server Edition) so that it can be easily accessed. NFS has not seemed to work at all with that, even with all the relevant changes in the Fedora Wiki and elsewhere, so I have settled on using Rsync within the Vagrantfile.

It should be noted that if NFS works with you, do use that as it is the preferred method.

## Prerequisites

* Git
* Vagrant (and its dependencies)
	* And vagrant-libvirt plugin
* Libvirt
* Qemu
* nfs-utils (and enable nfs-server) **ONLY IF USING NFS**

### Contraindictions And Solving Them
//TODO, make this bit more readable
* Make sure your user is in the `libvirt` group, if not run:
```sh
sudo usermod -a -G libvirt <user_name>
```
* Check if `libvirt` is running, if not run:
```sh
sudo systemctl enable libvirtd.service --now
```
* It will not work on `Fedora 28 Server Edition`, even with all the the prerequisites due to some missing dependencies with Vagrant
	* `Fedore 29 Server Edition` seems to break when trying to `vagrant ssh` into it with the following message below:
		* Cannot access manually with a basic `ssh` command too
```sh
Too many recursive configuration includes
```
* If the above option continues being shown, even upon upgrade, check within the `/etc/ssh/ssh_config/` folder. If you have any `.conf` files within it, make sure the following line is commented out or deleted (consider using `sed` or `awk` commands if one doesn't want to manually edit this file):
	* Remember to restart the ssh server with `systemctl restart sshd.service`
```sh
Include /etc/ssh/ssh_config/*.conf
```

* If getting `Name <vagrant_default> of domain about to create is already taken. Please try to run <vagrant up> command again.`, try:
```sh
sudo virsh list --all
# get the machine name
sudo virsh destroy <THE_MACHINE>
sudo virsh undefine <THE_MACHINE>
sudo virsh vol-list default
# get the volume name
sudo virsh vol-delete --pool default <THE_VOLUME>
```
* **N.B.** If it keeps happening, delete the relevant image in `~/.local/share/libvirt/images/`

## Getting Started

* Clone this repository using a `git clone` command
* Go into the directory and run
```sh
vagrant up --provider=libvirt
# or just as below since provider is specified in Vagrantfile
vagrant up
```
* This should have set up your Calibre server to be used, all that would be left is setting it up however you like within the GUI

## Vagrantfile
* The image is a generic Ubuntu 18.04 with a libvirt provider
```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "generic/ubuntu1804"
```

* Calibre webserver runs on port 8080 by default, though I have changed the host port to 8081, as 8080 clashes with Jenkins and other services
    * host_ip is my local server IP, so you would likely need to change this (this allows me to just type in the serverip:8081 and I get access to it, without having to mess around with more firewall networking rules)
```ruby
  config.vm.network "private_network", ip: "0.0.0.0"
  config.vm.network "forwarded_port", guest: 8080, host: 8081, host_ip: "192.168.0.24"
```

* Set the machine hostname to `calibre`
```ruby
  config.vm.hostname = "calibre"
```

* Start the syncing of the books directory across. If using NFS, can just remove everything after (and including) the second command
```ruby
  # syncs Books folder to vagrant calibre_library, make sure things don't change
  config.vm.synced_folder "/home/amaan/Books/", "/home/vagrant/Books", type: "rsync",
    rsync__args: ["--archive", "--verbose", "-z", "--progress"]
```

* Sets the default provider to `libvirt`, so it doesn't have to be specified when doing `vagrant up`
```ruby
   # set default provider to libvirt
  config.vm.provider "libvirt" do |lv|
    lv.memory = "1024"
    # to fix the vagrant up networking ip issue
    lv.qemu_use_session = false
  end
```

* Allow the ansible playbook to be used in the provisioning
```ruby
  config.vm.provision "ansible" do |ansible|
    ansible.verbose = "v"
    ansible.playbook = "provisioning/calibre_installation.yaml"
   end
end
```

## Ansible-Playbook

## Ease Of Life Changes On The Host Server

* One wants to start Calibre if the host machine is turned on and off again. The easiest way for this is to create a systemd module and enable it
* In `/etc/systemd/system/`, create a file called `calibre-server.service`, and in it enter:
```
TODO
```

## Manual Backups

* One way to backup everything within your calibre_library (including all metadata and covers etc), would be using an rsync command such as the one below:
```sh
# to find the ip address of this machine
vagrant ssh-config
# to copy the files from the VM to the host machine
rsync -avzr -e "ssh -i /location/of/repository/.vagrant/machines/default/libvirt/private_key" vagrant@machine_ip:/home/vagrant/calibre_library /location/of/backup/on/host/machine --progress
```

## Unresolvable Problems And Improvements


* **IF USING NFS:** `config.vm.synced_folder` option causes it to hang on `vagrant up`, cannot be solved at this time with any options (including checking for nfs installation, changing the version of what it uses, creating new firewall rules which seem to work for everyone else [see below]), so the syncing has been done with the ansible playbook
```sh
# these assume your zone is libvirt, you can create this zone and attach it to virbr0 which is for all virtualisation related things
 firewall-cmd  --permanent --zone=libvirt --add-service=nfs
 firewall-cmd  --permanent --zone=libvirt --add-service=mountd
 firewall-cmd  --permanent --zone=libvirt --add-service=rpc-bind
 firewall-cmd  --permanent --zone=libvirt --add-port=2049/tcp
 firewall-cmd  --permanent --zone=libvirt --add-port=2049/udp
 firewall-cmd --reload
```

# TODO:
- [ ] User authentication within the ansible setup
- [x] Get the ansible script to actually make the server start
- [x] Make into a systemd service (see above point of actually making it start)
- [x] Perhaps try using rsync within Vagrant instead of the copying folder across within Ansible
- [x] Use `when` conditionals in the Ansible playbook so that the installation file is not installed every time the playbook is run
- [x] Sort out the missing metadata situation (problem with mainly the pdfs, but with some epubs too)
  - looks like it is mostly done manually with the `fetch-ebook-metadata` commands, pipe to opf files and then use `calibredb set_metadata` commands
- [x] Collect only the non-pdf formats into the calibre-server, as pdfs seem to break in the reader, as well as mostly missing proper metadata
  - just specify in the adding books to library section or ignore the pattern for pdfs .e.g. `calibredb add --ignore *.pdf --recurse --with-library "{{ library }}"`
- [x] Backup method of the whole libraries
  - Restore method would just be in reverse
- [ ] Split up the playbook into many playbooks that are called from a main one
- [ ] POTENTIAL - Python script to fetch and implement all ebook metadata, that is used within the Ansible script
- [ ] POTENTIAL (less important) - restrict disk space usage, can use the vagrant-disksize plugin
